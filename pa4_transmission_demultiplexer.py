import sys

CHIPPING_SEQUENCE_ONE = [1, 1, -1, 1]
CHIPPING_SEQUENCE_TWO = [-1, 1, -1, -1]

SIGNAL_SIZE = 5
CHIP_SIZE = len(CHIPPING_SEQUENCE_ONE)

#Keys are string representation of list because tables
#cannot use lists as keys
DECODING_MAP = { '[1, 1, 1, 1, 1]':      'A',
                 '[1, 1, 1, 1, -1]':     'B',
                 '[1, 1, 1, -1, 1]':     'C',
                 '[1, 1, 1, -1, -1]':    'D',
                 '[1, 1, -1, 1, 1]':     'E',
                 '[1, 1, -1, 1, -1]':    'F',
                 '[1, 1, -1, -1, 1]':    'G',
                 '[1, 1, -1, -1, -1]':   'H',
                 '[1, -1, 1, 1, 1]':     'I',
                 '[1, -1, 1, 1, -1]':    'J',
                 '[1, -1, 1, -1, 1]':    'K',
                 '[1, -1, 1, -1, -1]':   'L',
                 '[1, -1, -1, 1, 1]':    'M',
                 '[1, -1, -1, 1, -1]':   'N',
                 '[1, -1, -1, -1, 1]':   'O',
                 '[1, -1, -1, -1, -1]':  'P',
                 '[-1, 1, 1, 1, 1]':     'Q',
                 '[-1, 1, 1, 1, -1]':    'R',
                 '[-1, 1, 1, -1, 1]':    'S',
                 '[-1, 1, 1, -1, -1]':   'T',
                 '[-1, 1, -1, 1, 1]':    'U',
                 '[-1, 1, -1, 1, -1]':   'V',
                 '[-1, 1, -1, -1, 1]':   'W',
                 '[-1, 1, -1, -1, -1]':  'X',
                 '[-1, -1, 1, 1, 1]':    'Y',
                 '[-1, -1, 1, 1, -1]':   'Z',
                 '[-1, -1, 1, -1, 1]':   ' ',
                 '[-1, -1, 1, -1, -1]':  ',',
                 '[-1, -1, -1, 1, 1]':   '.',
                 '[-1, -1, -1, 1, -1]':  "'",
                 '[-1, -1, -1, -1, 1]':  '?',
                 '[-1, -1, -1, -1, -1]': '!' }

def decodeCDMAToString(cdmaMessage):
    decodedString = ''

    for letter in cdmaMessage:
        #repr() turns a list into it's string interpretation
        decodedString += DECODING_MAP[repr(letter)]

    return decodedString

# Splits a list into a list with tuples of determined size.
#
# Ex.
#     list = [1, 2, 3, 4]
#     result = groupListIntoListOfTuples(list, 2)
#
# result would then be [(1, 2), (3, 4)]
def groupListIntoListOfTuples(l, groupLength):
    return list(zip( * (iter(l),) * groupLength))

def readRawTransmission(rawTransmission, chippingSequence):
    splitTransmission = rawTransmission.split(',')
    splitTransmission = groupListIntoListOfTuples(splitTransmission, len(chippingSequence))

    result = []
    letter = []

    for i in range(len(splitTransmission)):
        # Pulls a tuple from the split list representing a single signal
        signal = list(splitTransmission[i])
        demultiplexedSignal = 0

        for j in range(len(signal)):
            signalBit = int(signal[j])
            chippingBit = int(chippingSequence[j])

            demultiplexedSignal += signalBit * chippingBit

        demultiplexedSignal /= len(chippingSequence)

        letter.append(int(demultiplexedSignal))

        # Every SIGNAL_SIZE bits of the signal means a letter has been decoded
        if (i + 1) % SIGNAL_SIZE == 0:
            result.append(letter)
            letter = []

    return result

userInput = input('What file would you like to open? ')
infile = open(userInput, 'r')

inputString = infile.readline()

transmissionOne = readRawTransmission(inputString, CHIPPING_SEQUENCE_ONE)
transmissionTwo = readRawTransmission(inputString, CHIPPING_SEQUENCE_TWO)

messageOne = decodeCDMAToString(transmissionOne)
messageTwo = decodeCDMAToString(transmissionTwo)
print(messageOne)
print(messageTwo)

outfile = open('demultiplexed.txt', 'w')
outfile.write(messageOne + '\n\n' + messageTwo)

infile.close()
outfile.close()
